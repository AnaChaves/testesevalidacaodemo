package Model;

import java.util.TreeMap;
import java.util.Map;

public class Bookstore {

    private String name;
    private Map<String, Book> books;
    private Map<String, User> users;

    private User currentUser;

    public Bookstore() {
        books = new TreeMap<String, Book>();
        users = new TreeMap<String, User>();
        currentUser = null;

        books.put("1", new Book("1",  "Bruno Meira & Ana Carolina", "MBT is good"));
        books.put("2", new Book("2",  "Cristina Ferreira", "Pra cima de testes"));
        books.put("3", new Book("3", "Ana Paiva", "Como matar mutantes 1º edição for MESW 2020 students"));
        books.put("4", new Book("4", "Alexandre Dumas", "The Count of Monte Cristo"));
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void register(String identification, String name) {
        users.put(identification, new User(identification, name));
        System.out.println("Registration was sucessfull");
    }

    public void login(String identification) {
        User user = users.get(identification);
        if (user != null) {
            currentUser = user;
            user.printNotifications();
            user.clearNotification();
        }
        else {
            System.out.println("User with identification " + identification + " isn't registered in the system.");
        }
    }

    public void logout() {
        currentUser = null;
    }

    public void requestBook(String ISBN) {
        Book book = books.get(ISBN);
        if (book != null) {
            if (book.requestBook()) {
                System.out.println("Book with ISBN " + ISBN + " was requested with success.");
            } else {
                if (currentUser != null) {
                    if(book.getHasDefect()){
                        book.attach((Observer) currentUser);
                        System.out.println("Book with ISBN " + ISBN + " has a defect. After it's repaired you will you will be notified.");
                    }
                    else{
                        book.attach((Observer) currentUser);
                        System.out.println("Book with ISBN " + ISBN + " isn't available. When it will you will be notified.");
                    }
                }
            }
        }
        else {
            System.out.println("Book with ISBN " + ISBN + " doesn't exist.");
        }
    }

    public void deliverBook(String ISBN) {
        Book book = books.get(ISBN);
        if (book != null) {
            if (book.deliverBook()) {
                System.out.println("Book with ISBN " + ISBN + " was returned with success.");
            } else {
                System.out.println("You don't have books to deliver.");
            }
        }
        else {
            System.out.println("Book with ISBN " + ISBN + " doesn't exist.");
        }
    }

    public void printUsers(){
        System.out.println(users.values());
    }

    public void printState(String ISBN){
        Book book = books.get(ISBN);
        if (book != null) {
            book.printState();
        }
    }

    public void repair(String ISBN){
        Book book = books.get(ISBN);
        if (book != null) {
            book.repair();
        }
    }
}
