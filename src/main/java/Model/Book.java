package Model;

import java.util.ArrayList;
import java.util.List;

import BookState.AvailableState;
import BookState.BookState;

public class Book implements Subject {
    private String ISBN;
    private String author;
    private BookState state;
    private String title;
    private boolean hasDefect;
    private List<Observer> observers;

    public Book(String ISBN, String author, String title) {
        this.ISBN = ISBN;
        this.author = author;
        this.title = title;
        this.state = new AvailableState();
        this.hasDefect = false;
        this.observers = new ArrayList<Observer>();
    }

    public boolean getHasDefect() {
        return hasDefect;
    }

    public void setState(BookState state) {
        this.state = state;
    }

    public void notifyAllObservers(String notification) {
        for (Observer observer : observers) {
            User user = ((User) observer);
            user.addNotification(notification);
        }
    }


    public void attach(Observer observer) {
        observers.add(observer);
    }

    public void detach(Observer observer) {
        observers.remove(observer);
    }

    public boolean requestBook() {
        return this.state.request(this);
    }

    public void setHasDefect(boolean hasDefect) {
        this.hasDefect = hasDefect;
    }

    public boolean deliverBook() {

        if (this.state.deliver(this)) {
            if(this.hasDefect){
                notifyAllObservers("Book with ISBN " + this.ISBN + " is damaged. You'll be notified when it's repaired");
            }
            else{
                notifyAllObservers("Book with ISBN " + this.ISBN + " is already available");
                observers.clear();
            }
            return true;
        }
        return false;
    }

    public boolean repair() {
        if (this.state.repair(this)) {
            notifyAllObservers("Book with ISBN " + this.ISBN + " was repared and it's already available");
            observers.clear();
            return true;
        }
        return false;
    }

    public void printState(){
        System.out.println(this.state.getClass().getSimpleName());
    }
}
