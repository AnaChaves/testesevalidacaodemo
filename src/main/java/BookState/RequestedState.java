package BookState;

import Model.Book;

import java.util.Random;

public class RequestedState implements BookState {
    public boolean request(Book book) {
        return false;
    }

    public boolean deliver(Book book) {

        int low=0, high=10;
        Random random=new Random();
        int res=random.nextInt(((high-low)+1)-low);

        if(res>=0) {
            book.setHasDefect(true);
            book.setState(new DamagedState());
        }
        else
            book.setState(new AvailableState());
        return true;
    }


    public boolean repair(Book book) {
        return false;
    }
}
