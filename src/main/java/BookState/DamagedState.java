package BookState;

import Model.Book;

public class DamagedState implements BookState {

    public boolean request(Book book) {
        return false;
    }


    public boolean deliver(Book book) {
        return false;
    }

    public boolean repair(Book book){
        book.setHasDefect(false);
        book.setState(new AvailableState());
        return true;
    }
}
