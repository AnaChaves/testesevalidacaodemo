package BookState;

import Model.Book;

public interface BookState {
    boolean request(Book book);
    boolean deliver(Book book);
    boolean repair(Book book);
}
