package BookState;

import Model.Book;

public class AvailableState implements BookState {

    public boolean request(Book book) {
        book.setState(new RequestedState());
        return true;
    }

    public boolean deliver(Book book) {
        return false;
    }

    public boolean repair(Book book) {
        return false;
    }

}
