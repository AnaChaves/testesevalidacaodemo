package TestModels;

import Model.Book;
import Model.Bookstore;
import osmo.tester.annotation.AfterTest;
import osmo.tester.annotation.BeforeTest;
import osmo.tester.annotation.Guard;
import osmo.tester.annotation.TestStep;

public class BookState {

    private Bookstore bookstore;

    private static final String IDENTIFICATION = "user1@user1";
    private static final String NAME = "user1";

    private static final String IDENTIFICATION_2 = "user2@user2";
    private static final String NAME_2 = "user2";

    @BeforeTest
    public void startTest() {
        bookstore = new Bookstore();

        bookstore.register(IDENTIFICATION, NAME);
        bookstore.register(IDENTIFICATION_2, NAME_2);

        bookstore.logout();
        System.out.println("=============== TEST START ===============");
    }


    @Guard("login user2")
    public boolean login_user_2(){ return (bookstore.getCurrentUser()==null);}


    @TestStep("login user2")
    public void loginUser2() {
        System.out.println("\nLOGIN_USER_2");
        bookstore.login(IDENTIFICATION_2);
    }


    @Guard("requestBook_user2")
    public boolean requestBookUser2(){ return (bookstore.getCurrentUser()!=null) && bookstore.getCurrentUser().getIdentification()==IDENTIFICATION_2;}

    @TestStep("requestBook_user2")
    public void requestBook_user2() {
        System.out.println("\nREQUEST BOOK USER_2");
        bookstore.printState("4");
        bookstore.requestBook("4");
        bookstore.logout();
    }

    @Guard("login user1")
    public boolean login_user_1(){ return (bookstore.getCurrentUser()==null);}

    @TestStep("login user1")
    public void loginUser1() {
        System.out.println("\nLOGIN_USER_1");
        bookstore.login(IDENTIFICATION);
    }

    @Guard("deliverBook_user1")
    public boolean deliverBookUser1(){ return (bookstore.getCurrentUser()!=null) && bookstore.getCurrentUser().getIdentification()==IDENTIFICATION;}


    @TestStep("deliverBook_user1")
    public void deliver() {
        System.out.println("\nDELIVER USER_1");
        bookstore.printState("4");
        bookstore.deliverBook("4");
        bookstore.logout();
    }


    @TestStep("requestBook_user1")
    public void requestBook_user1() {
        System.out.println("\nREQUEST BOOK USER_1");
        bookstore.printState("4");
        bookstore.requestBook("4");
        bookstore.logout();
    }


    @TestStep("login user_2")
    public void login_user2() {
        System.out.println("\nLOGIN USER_2");
        bookstore.login(IDENTIFICATION_2);
    }

    @AfterTest
    public void endTest() {
        System.out.println("================ TEST END ================");
    }
}
