package TestModels;

import Model.Bookstore;
import osmo.tester.annotation.AfterTest;
import osmo.tester.annotation.BeforeTest;
import osmo.tester.annotation.Guard;
import osmo.tester.annotation.TestStep;

public class RepairBook {

    private Bookstore bookstore;

    private static final String IDENTIFICATION = "user1@user1";
    private static final String NAME = "user";

    @BeforeTest
    public void startTest() {
        bookstore = new Bookstore();

        bookstore.register(IDENTIFICATION, NAME);

        System.out.println("=============== TEST START ===============");
    }

    @Guard("login_user")
    public boolean login_user(){ return (bookstore.getCurrentUser()==null);}

    @TestStep("login_user")
    public void loginUser() {
        System.out.println("\nLOGIN_USER");
        bookstore.login(IDENTIFICATION);
    }

    @Guard("deliverBook_user")
    public boolean deliverBookUser(){ return (bookstore.getCurrentUser()!=null);}

    @TestStep("deliverBook_user")
    public void deliver() {
        System.out.println("\nDELIVER BOOK");
        bookstore.printState("4");
        bookstore.deliverBook("4");
    }

    @Guard("logout")
    public boolean allowLogout() {
        return (bookstore.getCurrentUser() != null);
    }

    @TestStep("logout")
    public void logout() {
        System.out.println("LOGOUT");
        bookstore.logout();
    }

    @TestStep("requestBook_user")
    public void requestBook_user() {
        System.out.println("\nREQUEST BOOK");
        bookstore.printState("4");
        bookstore.requestBook("4");
    }

    @TestStep("repairBook4")
    public void repair() {
        System.out.println("\nREPAIR BOOK 4");
        bookstore.printState("4");
        bookstore.repair("4");
    }

    @AfterTest
    public void endTest() {
        System.out.println("================ TEST END ================");
    }
}
