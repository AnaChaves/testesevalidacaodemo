package Test;

import TestModels.BookState;
import osmo.tester.*;
import osmo.tester.generator.endcondition.Length;
import osmo.tester.scenario.Scenario;

public class TestBookState {
    public static void main(String[] args) {
        Scenario scenario = new Scenario(false);
        //scenario.addStartup("login user1", "requestBook_user1", "login user2");
        scenario.addStartup("login user1", "requestBook_user1", "login user2", "requestBook_user2", "login user1",
                "deliverBook_user1", "login user2");


        OSMOTester tester = new OSMOTester();
        tester.addModelObject(new BookState());
        tester.getConfig().setScenario(scenario);
        tester.setTestEndCondition(new Length(10));
        tester.setSuiteEndCondition(new Length(8));
        tester.generate(52);
    }

}
