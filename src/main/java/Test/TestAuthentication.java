package Test;

import TestModels.Authentication;
import osmo.tester.OSMOTester;
import osmo.tester.generator.endcondition.Length;
import osmo.tester.scenario.Scenario;

public class TestAuthentication {
    public static void main(String[] args) {
        Scenario scenario = new Scenario(false);
        //scenario.addStartup("register", "login", "logout");

        OSMOTester tester = new OSMOTester();
        tester.addModelObject(new Authentication());
        //tester.getConfig().setScenario(scenario);
        tester.setTestEndCondition(new Length(6));
        tester.setSuiteEndCondition(new Length(8));
        tester.generate(52);
    }

}
