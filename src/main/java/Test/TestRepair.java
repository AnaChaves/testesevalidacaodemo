package Test;

import TestModels.RepairBook;
import osmo.tester.OSMOTester;
import osmo.tester.generator.endcondition.Length;
import osmo.tester.scenario.Scenario;

public class TestRepair {
    public static void main(String[] args) {
        Scenario scenario = new Scenario(false);
        scenario.addStartup("login_user", "requestBook_user", "logout", "login_user", "deliverBook_user", "logout",
                "login_user", "requestBook_user", "logout", "repairBook4", "login_user");

        OSMOTester tester = new OSMOTester();
        tester.addModelObject(new RepairBook());
        tester.getConfig().setScenario(scenario);
        tester.setTestEndCondition(new Length(20));
        tester.setSuiteEndCondition(new Length(8));
        tester.generate(52);
    }

}
